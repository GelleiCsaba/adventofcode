class Prg:
    def main(self):
        ssum = 0
        with open('1.in', 'r') as f:
            lines = f.readlines()
            for item in lines:
                ssum += (int(item) // 3) - 2;
        print(ssum);

Prg().main();
